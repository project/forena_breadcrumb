<?php

/**
 * @file
 * Implements a title renderer.
 */

/**
 * {@inheritdoc}
 */
class FrxBreadcrumb extends FrxRenderer {

  /**
   * {@inheritdoc}
   */
  public function render() {

    $frxBreadcrumb = &drupal_static('FrxBreadcrumb');
    $frxBreadcrumb = $this->frxReport->title;

    return '';
  }

}
